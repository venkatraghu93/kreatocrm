package KreatoTC;

/*import java.text.SimpleDateFormat;
import java.util.Date;*/
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class BdayAnnMailService {	

		@Test
		public void testMethod() throws Exception {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().deleteAllCookies();	
		
		driver.navigate().to("https://testrun.kreatocrm.com");

//Login		
		driver.findElementById("Login_txtUserName").clear();
		driver.findElementById("Login_txtUserName").sendKeys("saravana");
		driver.findElementById("Login_txtPassword").clear();
		driver.findElementById("Login_txtPassword").sendKeys("1234");
		driver.findElementById("Login_btnLogin").click();
		
//Click Contacts Module from Customers		
		Actions builder = new Actions(driver);
		WebElement eleCustomers = driver.findElementByXPath("//a[text()='Customers']");
		builder.moveToElement(eleCustomers).perform();
		WebDriverWait wait = new WebDriverWait(driver, 20);
		WebElement elecontacts = driver.findElementByXPath("//a[text()='Contacts']");
		wait.until(ExpectedConditions.elementToBeClickable(elecontacts)).click();
		
//Search and select contact name from module search		
		driver.findElementById("ctl00_ContentPlaceHolder1_cbpSubContent_txtSearch_I") .sendKeys("abi", Keys.ENTER);
		Thread.sleep(5000);
		driver.findElementById("ctl00_ContentPlaceHolder1_cbpSubContent_cbpDynamicList_grvDynamicResult_tccell0_2").click();
			
//Edit the contact and update the Birthday and Anniversary field		
		driver.findElementByXPath("//span[text()='EDIT']").click();
		
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElementById("ctl00_ContentPlaceHolder1_cbpAssociationItems_panelAssociateDetails_panelEditPage_panelAssnDetailEdit_Birthday_B-1"))).click();
		driver.findElementById("ctl00_ContentPlaceHolder1_cbpAssociationItems_panelAssociateDetails_panelEditPage_panelAssnDetailEdit_Birthday_DDD_C_BT").click();
		
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElementById("ctl00_ContentPlaceHolder1_cbpAssociationItems_panelAssociateDetails_panelEditPage_panelAssnDetailEdit_Anniversary_B-1"))).click();
		driver.findElementById("ctl00_ContentPlaceHolder1_cbpAssociationItems_panelAssociateDetails_panelEditPage_panelAssnDetailEdit_Anniversary_DDD_C_BT").click();
		
		driver.findElementByXPath("(//span[text()='Save'])[1]").click();
		
//Click Logout		
		driver.findElementByXPath("(//span[@class='simplearrow'])[2]").click();
		driver.findElementById("ctl00_lnkLogOut").click();
		driver.close();
		
	}

}
		
/*		
 		WebElement eleAnniversary = driver.findElementById("ctl00_ContentPlaceHolder1_cbpAssociationItems_panelAssociateDetails_panelAssnDetailView_lblAnniversary");
		builder.moveToElement(eleAnniversary).perform();
		driver.findElementById("spanEditform").click();	
		
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd");
		String today = sdf.format(date);
		int tomorrow = Integer.parseInt(today)+1;
		
		System.out.println(tomorrow);
						
				driver.findElementByXPath("//span[text()='EDIT']").click();
				wait.until(ExpectedConditions.elementToBeClickable(driver.findElementById("ctl00_ContentPlaceHolder1_cbpAssociationItems_panelAssociateDetails_panelEditPage_panelAssnDetailEdit_Anniversary_B-1"))).click();
				wait.until(ExpectedConditions.elementToBeClickable(driver.findElementById("ctl00_ContentPlaceHolder1_cbpAssociationItems_panelAssociateDetails_panelEditPage_panelAssnDetailEdit_Anniversary_DDD_C_BC"))).click();
				wait.until(ExpectedConditions.elementToBeClickable(driver.findElementById("ctl00_ContentPlaceHolder1_cbpAssociationItems_panelAssociateDetails_panelEditPage_panelAssnDetailEdit_Anniversary_B-1"))).click();
				
				String beforeXpath = "//table[@id='ctl00_ContentPlaceHolder1_cbpAssociationItems_panelAssociateDetails_panelEditPage_panelAssnDetailEdit_Anniversary_DDD_C_mt']//tr/td[text()='";
				String afterXpath = "']";
				driver.findElementByXPath(beforeXpath+tomorrow+afterXpath).click();
		
		
*/